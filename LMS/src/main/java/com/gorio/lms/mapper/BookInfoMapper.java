package com.gorio.lms.mapper;

import com.gorio.lms.entities.BookEntity;
import com.gorio.lms.entities.BookInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface BookInfoMapper {

    int getCountOfIsbn(String isbn);
    int getIdByIsbn(String isbn);
    BookInfo getInfoByIsbn(String isbn);
    //加入书,为对属性值进行校验，如果需要检验每个属性的值，则使用insertSelective
    void addBookInfo(BookInfo record);
    //根据id进行查找书的信息
//    BookInfo findById(Integer id);
    List<BookInfo> getAllBook();

    BookInfo findById(Integer id);

    int BookCount();

    //TODO
    int deleteByPrimaryKey(Integer id);
    //TODO
    int insertSelective(BookInfo record);


    int updateByPrimaryKeySelective(BookInfo record);

    int updateByPrimaryKey(BookInfo record);

}