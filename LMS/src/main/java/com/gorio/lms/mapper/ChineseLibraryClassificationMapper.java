package com.gorio.lms.mapper;

import com.gorio.lms.entities.ChineseLibraryClassification;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface ChineseLibraryClassificationMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ChineseLibraryClassification record);

    int insertSelective(ChineseLibraryClassification record);

    ChineseLibraryClassification selectByCode(String code);

    ChineseLibraryClassification selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ChineseLibraryClassification record);

    int updateByPrimaryKey(ChineseLibraryClassification record);
}