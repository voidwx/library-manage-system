package com.gorio.lms.mapper;

import com.gorio.lms.entities.PersonEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface PersonEntityMapper {
    PersonEntity selectByStudentId(String id);

    int deleteByPrimaryKey(Integer id);

    int insert(PersonEntity record);

    int insertSelective(PersonEntity record);

    PersonEntity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PersonEntity record);

    int updateByPrimaryKey(PersonEntity record);

    List<PersonEntity> getAllPerson();


}