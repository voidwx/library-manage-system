package com.gorio.lms.mapper;

import com.gorio.lms.entities.Booklibrary;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface BooklibraryMapper {
    Booklibrary selectByIsbn(String isbn);

    //更新分类信息，当其输入isbn得到分类号，插入type和id
    void updateByClassifyType(String type , String isbn , int id);


    int deleteByPrimaryKey(Long id);

    int insert(Booklibrary record);

    int insertSelective(Booklibrary record);

    Booklibrary selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Booklibrary record);

    int updateByPrimaryKey(Booklibrary record);
}