package com.gorio.lms.mapper;

import com.gorio.lms.entities.BookEntity;
import com.gorio.lms.entities.BookInfo;
import com.gorio.lms.entities.PersonEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface BookEntityMapper {

    //加入书实体
    void addBookEntities(BookEntity bookEntity);

    //找人
    PersonEntity findPersonByStudentId(String StudentId);

    //找bookinfo的id
    BookInfo findBookInfoBtIsbn(String ISBN);

    //找书信息
    BookEntity findByBookEntitiesId(Integer bookId);

    List<BookEntity> findAllEntities();

    List<BookEntity> findBystudentId(String studentId);

    int getBookCount();

//    int insertSelective(BookEntity record);
//
//    int deleteByPrimaryKey(Integer bookId);
//
//
//
//
//    int updateByPrimaryKeySelective(BookEntity record);
//
//    int updateByPrimaryKey(BookEntity record);
}