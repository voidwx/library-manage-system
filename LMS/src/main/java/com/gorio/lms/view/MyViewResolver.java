package com.gorio.lms.view;

import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;

import java.util.Locale;

/**
 * Project Name spring-boot-01-helloworld
 * Package Name com.gorio.xdce.view
 * Class Name MyViewResolver
 * Created by Gorio
 *
 * @author Gorio
 * @date 2020/9/13 11:31
 */
public class MyViewResolver implements ViewResolver {
    @Override
    public View resolveViewName(String s, Locale locale) throws Exception {
        return null;
    }
}
