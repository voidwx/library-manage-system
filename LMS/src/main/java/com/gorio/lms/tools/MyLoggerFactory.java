package com.gorio.lms.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Project Name spring-boot-01-helloworld
 * Package Name com.gorio.xdce.tools
 * Class Name LoggerFactory
 * Created by Gorio
 *
 * @author Gorio
 * @date 2020/9/13 18:18
 */
public class MyLoggerFactory {

    public static Logger getLogger(Class clz) {
        return LoggerFactory.getLogger(clz);
    }
}
