package com.gorio.lms.tools;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class JdbcUtils {
    private static Connection conn = null;
    public static Map<Integer, Integer> map = new HashMap<>();
    public static Connection getConn(){
        PropertiesUtil.loadFile("jdbc.properties");
        String driver = PropertiesUtil.getPropertyValue("driver");
        String url = PropertiesUtil.getPropertyValue("url");
        String username  = PropertiesUtil.getPropertyValue("username");
        String password = PropertiesUtil.getPropertyValue("password");
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url,username,password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
            close();
        }
        return conn;
    }
    public static void close(){
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static int getLevelById(int id){
        if (map.containsKey(id)){
            return map.get(id);
        }
        Connection conn = getConn();
        String sql = "select level from db_lms.chinese_library_classification where id = " + id + ";";
        Statement stmt = null;
        ResultSet ret = null;
        int result = -2;

        try {
            stmt = conn.createStatement();
            ret = stmt.executeQuery(sql);
            while (ret.next()){
                result = ret.getInt("level");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        finally {
            try {
                if (ret != null)
                    ret.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        }
        map.put(id, result);
        return result;
    }
    public static int getIdByCode(String code){
        Connection conn = getConn();
        String sql = "select id from db_lms.chinese_library_classification where code = '" + code + "';";
        Statement stmt = null;
        ResultSet ret = null;
        int result = -2;
        try {
            stmt = conn.createStatement();
            ret = stmt.executeQuery(sql);
            while (ret.next()){
                result = ret.getInt("id");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        finally {
            try {
                if (ret != null)
                    ret.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return result;
    }
    public static void executeSQL(String sql){
        Connection conn = getConn();
        Statement stmt = null;

        try {
            stmt = conn.createStatement();
            stmt.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

    }
}
