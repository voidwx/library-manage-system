package com.gorio.lms.tools;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

public class RandomGenerator {
    public static String randomString(final int count){
        return RandomStringUtils.randomAlphabetic(count);
    }
    public static String randomString(final int minLengthInclusive, final int maxLengthExclusive){
        return RandomStringUtils.randomAlphabetic(minLengthInclusive, maxLengthExclusive);
    }
    public static int randomInteger(){
        return RandomUtils.nextInt();
    }
    public static int randomInteger(final int startInclusive, final int endExclusive){
        return RandomUtils.nextInt(startInclusive, endExclusive);
    }
}
