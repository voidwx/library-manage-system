package com.gorio.lms.config;

import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.boot.autoconfigure.ConfigurationCustomizer;
import org.springframework.context.annotation.Configuration;

/**
 * Project Name spring-boot-01-helloworld
 * Package Name com.gorio.xdce.config
 * Class Name MyBatisConfig
 * Created by Gorio
 *
 * @author Gorio
 * @date 2020/9/16 16:13
 */
@MapperScan(value = "com.gorio.lms.mapper")
@Configuration
public class MyBatisConfig {

    public ConfigurationCustomizer configurationCustomizer() {
        return new ConfigurationCustomizer() {
            @Override
            public void customize(org.apache.ibatis.session.Configuration configuration) {
                configuration.setMapUnderscoreToCamelCase(true);
            }
        };
    }
}
