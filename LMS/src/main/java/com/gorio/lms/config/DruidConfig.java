package com.gorio.lms.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
//import com.gorio.xdce.entities.Admin;
import com.gorio.lms.entities.Admin;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


/**
 * Project Name spring-boot-01-helloworld
 * Package Name com.gorio.xdce.config
 * Class Name DruidConfig
 * Created by Gorio
 *
 * @author Gorio
 * @date 2020/9/15 22:52
 */
@Configuration
public class DruidConfig {


    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean
    public DataSource druid() {
        DataSource dataSource = new DruidDataSource();
        return dataSource;
    }

    //配置Druid的监控
    //1、配置一个管理后台servlet

    @Bean
    public ServletRegistrationBean statViewServlet() {
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(new StatViewServlet(), "/druid/*");
        Map<String, String> initParam = new HashMap<>();

        initParam.put("loginUsername", Admin.getInstance().getAdminAccount());
        initParam.put("loginPassword", Admin.getInstance().getAdminPassword());
        initParam.put("allow", "");
        //默认是允许所有
        registrationBean.setInitParameters(initParam);
        return registrationBean;
    }
    //2、配置一个监控的filter

    @Bean
    public FilterRegistrationBean webStatFilter() {
        FilterRegistrationBean bean = new FilterRegistrationBean();

        bean.setFilter(new WebStatFilter());


        Map<String, String> initParam = new HashMap<>();
        initParam.put("exclusions", "*.js,*.css,/druid/*");


        bean.setInitParameters(initParam);

        bean.setUrlPatterns(Arrays.asList("/*"));
        return bean;
    }
}
