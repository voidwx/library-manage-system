package com.gorio.lms.config;


import com.gorio.lms.component.LoginHandlerInterceptor;
import com.gorio.lms.component.MyLocaleResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Project Name spring-boot-01-helloworld
 * Package Name com.gorio.xdce.config
 * Class Name MyAppConfig
 * Created by Gorio
 *
 * @author Gorio
 * @date 2020/9/12 19:00
 */

@Configuration
public class MyAppConfig extends WebMvcConfigurerAdapter {
    /**
     * 默认ID是方法名
     */
//    @Bean
//    public HelloService helloService(){
//        System.out.println("adding HelloService..");
//        return new HelloService();
//    }
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/sayHello").setViewName("success");

    }


    @Bean
    public WebMvcConfigurerAdapter webMvcConfigurerAdapter() {
        WebMvcConfigurerAdapter adapter = new WebMvcConfigurerAdapter() {
            @Override
            public void addViewControllers(ViewControllerRegistry registry) {
                registry.addViewController("/").setViewName("login");
                registry.addViewController("/index").setViewName("login");
                registry.addViewController("/index.html").setViewName("login");
                registry.addViewController("/main.html").setViewName("dashboard");

            }
//            @Override
//            public void addInterceptors(InterceptorRegistry registry) {
//                registry.addInterceptor(new LoginHandlerInterceptor()).addPathPatterns("/**")
//                        .excludePathPatterns("/","/index","/index.html","/user/login","/login.html");
//            }
        };
        return adapter;
    }


    @Bean
    public LocaleResolver localeResolver() {
        return new MyLocaleResolver();
    }
}
