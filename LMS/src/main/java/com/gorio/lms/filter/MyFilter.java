package com.gorio.lms.filter;


import com.gorio.lms.tools.MyLoggerFactory;
import org.slf4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Project Name spring-boot-01-helloworld
 * Package Name com.gorio.xdce.filter
 * Class Name MyFilter
 * Created by Gorio
 *
 * @author Gorio
 * @date 2020/9/15 15:05
 */
public class MyFilter implements Filter {
    public static Logger logger = MyLoggerFactory.getLogger(MyFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        logger.info("MyFilter Process");
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        Object user = request.getSession().getAttribute("loginUser");
        if (user == null) {
            request.setAttribute("msg", "没有权限，请先登录");

            request.getRequestDispatcher("/index").forward(request, servletResponse);
            logger.error("MyFilter redirect {} to /index, cause by no login user", request.getRequestURI());
        } else {
            //@fixme redis
            //已登录。放行
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
