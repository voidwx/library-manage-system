//package com.gorio.lms.controller;
//
//import com.gorio.xdce.entities.AuthenticationResult;
//import com.gorio.xdce.mapper.AuthenticationResultMapper;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//
//import java.util.List;
//
///**
// * Project Name spring-boot-01-helloworld
// * Package Name com.gorio.xdce.controller
// * Class Name AuthenticationResultController
// * Created by Gorio
// *
// * @author Gorio
// * @date 2020/9/17 14:28
// */
//@Controller
//public class AuthenticationResultController {
//
//    @Autowired
//    AuthenticationResultMapper authenticationResultMapper;
//
//    @GetMapping("/results")
//    public String list(Model model){
//        List<AuthenticationResult> collection = authenticationResultMapper.getAllAuthenticationResult();
//        model.addAttribute("emps", collection);
//        return "result/list";
//    }
//
//}
