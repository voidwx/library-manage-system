package com.gorio.lms.controller;

import com.gorio.lms.entities.BookInfo;
import com.gorio.lms.entities.PersonEntity;
import com.gorio.lms.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class BookAddv1Controller {

    @Autowired
    BookInfoMapper bookInfoMapper;

    @Autowired
    BookEntityMapper bookEntityMapper;

    @Autowired
    BooklibraryMapper booklibraryMapper;

    @Autowired
    ChineseLibraryClassificationMapper chineseLibraryClassificationMapper;

    @Autowired
    PersonEntityMapper personEntityMapper;

    //应该将add页面进行更改，这个页面为中间展示界面
//    @GetMapping("/allinfobook")
//    public String addAllInfoBook(Model model){
//
//        return "book/allinfoadd";
//    }
}
