package com.gorio.lms.controller;

import com.gorio.lms.entities.BookEntity;
import com.gorio.lms.entities.BookInfo;
import com.gorio.lms.entities.PersonEntity;
import com.gorio.lms.mapper.PersonEntityMapper;
import com.gorio.lms.tools.MyLoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Controller
public class PeopleController {
    static Logger logger = MyLoggerFactory.getLogger(PeopleController.class);

    @Autowired
    PersonEntityMapper personEntityMapper;

//    /*
//   TODO
//   */
    @GetMapping("/people")
    public String list(Model model){
        List<PersonEntity> list = personEntityMapper.getAllPerson();
//        System.out.println("student1:"+list.get(0).getStudentId());
//        System.out.println("list1:"+list);
        model.addAttribute("emps",list);

        return "person/list";
    }

    @GetMapping("/person/{id}")
    public String moreInfo(@PathVariable("id") Integer id, Model model){
        id = Integer.valueOf(id);
        PersonEntity personEntity = personEntityMapper.selectByPrimaryKey(id);
        if (personEntity == null){
            logger.error("no such book");
            return "redirect:/people";
        }
        model.addAttribute("emp",personEntity);
        return "person/add";

    }

    //
    @GetMapping("/person")
    public String addPerson(Model model) { return "person/add"; }

    /*add BookInfo*/
    @PostMapping("/person")
    public String addPersonInfo(PersonEntity personEntity, Map<String, Object> map) {
        logger.info("保存的人员信息：{}", personEntity);

        if (!checkPerson(personEntity, map)) {
            return "redirect:/people";
        }
        //System.out.println("------");
        personEntityMapper.insert(personEntity);
        return "redirect:/people";
    }

    @PutMapping("/person")
    public String SavePersonInfo(PersonEntity personEntity, Map<String, Object> map) {
        logger.info("修改的人员信息：{}", personEntity);

        if (!checkPerson(personEntity, map)) {
            return "redirect:/people";
        }
        //System.out.println("------");
        personEntityMapper.updateByPrimaryKey(personEntity);
        return "redirect:/people";
    }

//    @ResponseBody
//    @GetMapping(value = "/bookinfo/")
//    public String getBookInfo(@RequestParam int id) {
////        BookInfo info = new BookInfo();
//
//        BookEntity bookEntity = new BookEntity();
//        bookEntity = bookEntityMapper.findByBookEntitiesId(1);
//        //search
//        return bookEntity.toString();
//    }

    private Boolean checkPerson(PersonEntity personEntity, Map<String, Object> map) {
        return true;
    }
}
