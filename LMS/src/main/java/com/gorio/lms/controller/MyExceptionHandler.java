package com.gorio.lms.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Project Name spring-boot-01-helloworld
 * Package Name com.gorio.xdce.controller
 * Class Name MyExpectionHandler
 * Created by Gorio
 *
 * @author Gorio
 * @date 2020/9/14 22:09
 */
@ControllerAdvice
public class MyExceptionHandler {


    //fixme
    @ExceptionHandler(Exception.class)
    public String handlerException(Exception e, HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();

        request.setAttribute("javax.servlet.error.status_code", 400);

        map.put("code", "400");
        map.put("message", e.getMessage());
        request.setAttribute("ext", map);
        return "forward:/error";
    }
}
