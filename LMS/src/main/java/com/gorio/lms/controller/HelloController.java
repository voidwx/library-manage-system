package com.gorio.lms.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Project Name spring-boot-01-helloworld
 * Package Name com.gorio.xdce.controller
 * Class Name HelloController
 * Created by Gorio
 *
 * @author Gorio
 * @date 2020/9/12 16:56
 */
@Controller
public class HelloController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @GetMapping({"/", "/index.html"})
    @PostMapping({"/", "/index.html"})
    public String index() {
        return "login";
    }

}
