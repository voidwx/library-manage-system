package com.gorio.lms.entities;

public class BookInfoTemp {
    private Integer id;

    private String bookName;

    private String bookAuthor;

    private String bookPrinter;

    private String bookVersion;

    private String bookPrinterVersion;

    private String bookCover;

    private String bookIsbn;

    private Double bookPrice;

    private Integer bookInboundNumber;

    private String bookRecommend;   //中图分类号

    private String bookAbstract;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public String getBookPrinter() {
        return bookPrinter;
    }

    public void setBookPrinter(String bookPrinter) {
        this.bookPrinter = bookPrinter;
    }

    public String getBookVersion() {
        return bookVersion;
    }

    public void setBookVersion(String bookVersion) {
        this.bookVersion = bookVersion;
    }

    public String getBookPrinterVersion() {
        return bookPrinterVersion;
    }

    public void setBookPrinterVersion(String bookPrinterVersion) {
        this.bookPrinterVersion = bookPrinterVersion;
    }

    public String getBookCover() {
        return bookCover;
    }

    public void setBookCover(String bookCover) {
        this.bookCover = bookCover;
    }

    public String getBookIsbn() {
        return bookIsbn;
    }

    public void setBookIsbn(String bookIsbn) {
        this.bookIsbn = bookIsbn;
    }

    public Double getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(Double bookPrice) {
        this.bookPrice = bookPrice;
    }

    public Integer getBookInboundNumber() {
        return bookInboundNumber;
    }

    public void setBookInboundNumber(Integer bookInboundNumber) {
        this.bookInboundNumber = bookInboundNumber;
    }

    public String getBookRecommend() {
        return bookRecommend;
    }

    public void setBookRecommend(String bookRecommend) {
        this.bookRecommend = bookRecommend;
    }

    public String getBookAbstract() {
        return bookAbstract;
    }

    public void setBookAbstract(String bookAbstract) {
        this.bookAbstract = bookAbstract;
    }

    @Override
    public String toString() {
        return "BookInfoTemp{" +
                "id=" + id +
                ", bookName='" + bookName + '\'' +
                ", bookAuthor='" + bookAuthor + '\'' +
                ", bookPrinter='" + bookPrinter + '\'' +
                ", bookVersion='" + bookVersion + '\'' +
                ", bookPrinterVersion='" + bookPrinterVersion + '\'' +
                ", bookCover='" + bookCover + '\'' +
                ", bookIsbn='" + bookIsbn + '\'' +
                ", bookPrice=" + bookPrice +
                ", bookInboundNumber=" + bookInboundNumber +
                ", bookRecommend=" + bookRecommend +
                ", bookAbstract='" + bookAbstract + '\'' +
                '}';
    }
}
