package com.gorio.lms.entities;

public class AllInfoOfBook {
    private String isbn;
    //学号
    private String owner;
    private Integer status;
    private String location;
//    private String note;

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

//    public String getNote() {
//        return note;
//    }
//
//    public void setNote(String note) {
//        this.note = note;
//    }

    @Override
    public String toString() {
        return "AllInfoOfBook{" +
                "isbn='" + isbn + '\'' +
                ", owner='" + owner + '\'' +
                ", status=" + status +
                ", location='" + location + '\'' +
                '}';
    }
}
