package com.gorio.lms.entities;

import com.google.gson.Gson;

import java.io.Serializable;

public class BookInfo implements Serializable {
    private Integer id;

    private String bookName;

    private String bookAuthor;

    private String bookPrinter;

    private String bookVersion;

    private String bookPrinterVersion;

    private String bookCover;

    private String bookIsbn;

    private Double bookPrice;

    private Integer bookInboundNumber;
    //@fixme bookRecommend --> ChineseLibraryClassification
    private ChineseLibraryClassification bookRecommend;

    private String bookAbstract;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public String getBookPrinter() {
        return bookPrinter;
    }

    public void setBookPrinter(String bookPrinter) {
        this.bookPrinter = bookPrinter;
    }

    public String getBookVersion() {
        return bookVersion;
    }

    public void setBookVersion(String bookVersion) {
        this.bookVersion = bookVersion;
    }

    public String getBookPrinterVersion() {
        return bookPrinterVersion;
    }

    public void setBookPrinterVersion(String bookPrinterVersion) {
        this.bookPrinterVersion = bookPrinterVersion;
    }

    public String getBookCover() {
        return bookCover;
    }

    public void setBookCover(String bookCover) {
        this.bookCover = bookCover;
    }

    public String getBookIsbn() {
        return bookIsbn;
    }

    public void setBookIsbn(String bookIsbn) {
        this.bookIsbn = bookIsbn;
    }

    public Double getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(Double bookPrice) {
        this.bookPrice = bookPrice;
    }

    public Integer getBookInboundNumber() {
        return bookInboundNumber;
    }

    public void setBookInboundNumber(Integer bookInboundNumber) {
        this.bookInboundNumber = bookInboundNumber;
    }

    public ChineseLibraryClassification getBookRecommend() {
        return bookRecommend;
    }

    public void setBookRecommend(ChineseLibraryClassification bookRecommend) {
        this.bookRecommend = bookRecommend;
    }

    public String getBookAbstract() {
        return bookAbstract;
    }

    public void setBookAbstract(String bookAbstract) {
        this.bookAbstract = bookAbstract;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        String result = gson.toJson(this);
        return result;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        BookInfo other = (BookInfo) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getBookName() == null ? other.getBookName() == null : this.getBookName().equals(other.getBookName()))
                && (this.getBookAuthor() == null ? other.getBookAuthor() == null : this.getBookAuthor().equals(other.getBookAuthor()))
                && (this.getBookPrinter() == null ? other.getBookPrinter() == null : this.getBookPrinter().equals(other.getBookPrinter()))
                && (this.getBookVersion() == null ? other.getBookVersion() == null : this.getBookVersion().equals(other.getBookVersion()))
                && (this.getBookPrinterVersion() == null ? other.getBookPrinterVersion() == null : this.getBookPrinterVersion().equals(other.getBookPrinterVersion()))
                && (this.getBookCover() == null ? other.getBookCover() == null : this.getBookCover().equals(other.getBookCover()))
                && (this.getBookIsbn() == null ? other.getBookIsbn() == null : this.getBookIsbn().equals(other.getBookIsbn()))
                && (this.getBookPrice() == null ? other.getBookPrice() == null : this.getBookPrice().equals(other.getBookPrice()))
                && (this.getBookInboundNumber() == null ? other.getBookInboundNumber() == null : this.getBookInboundNumber().equals(other.getBookInboundNumber()))
                && (this.getBookRecommend() == null ? other.getBookRecommend() == null : this.getBookRecommend().equals(other.getBookRecommend()))
                && (this.getBookAbstract() == null ? other.getBookAbstract() == null : this.getBookAbstract().equals(other.getBookAbstract()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getBookName() == null) ? 0 : getBookName().hashCode());
        result = prime * result + ((getBookAuthor() == null) ? 0 : getBookAuthor().hashCode());
        result = prime * result + ((getBookPrinter() == null) ? 0 : getBookPrinter().hashCode());
        result = prime * result + ((getBookVersion() == null) ? 0 : getBookVersion().hashCode());
        result = prime * result + ((getBookPrinterVersion() == null) ? 0 : getBookPrinterVersion().hashCode());
        result = prime * result + ((getBookCover() == null) ? 0 : getBookCover().hashCode());
        result = prime * result + ((getBookIsbn() == null) ? 0 : getBookIsbn().hashCode());
        result = prime * result + ((getBookPrice() == null) ? 0 : getBookPrice().hashCode());
        result = prime * result + ((getBookInboundNumber() == null) ? 0 : getBookInboundNumber().hashCode());
        result = prime * result + ((getBookRecommend() == null) ? 0 : getBookRecommend().hashCode());
        result = prime * result + ((getBookAbstract() == null) ? 0 : getBookAbstract().hashCode());
        return result;
    }
}