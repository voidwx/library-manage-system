package com.gorio.lms.entities;

import com.google.gson.Gson;
import com.gorio.lms.tools.Cipher;
import com.gorio.lms.tools.MyLoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

/**
 * Project Name spring-boot-01-helloworld
 * Package Name com.gorio.xdce.entities
 * Class Name Admin
 * Created by Gorio
 *
 * @author Gorio
 * @date 2020/9/13 16:24
 */

public class Admin {
    private String adminAccount;
    private String adminPassword;

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

    public String getAdminAccount() {
        return adminAccount;
    }

    public void setAdminAccount(String adminAccount) {
        this.adminAccount = adminAccount;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "adminAccount='" + adminAccount + '\'' +
                ", adminPassword='" + adminPassword + '\'' +
                '}';
    }

    public boolean authentication(String userName, String password, String time) {
        Logger log = MyLoggerFactory.getLogger(Admin.class);
        Long recvTime = Long.parseLong(time);
        Long nowTime = System.currentTimeMillis();
//        if (recvTime > nowTime){
//            //时戳时间不能晚于现在时间
//            log.error("user {} auth error time is after now,by password = '{}', time ={} at {}({})", userName,
//                    password, time, new Date().toString(), nowTime);
//            return false;
//        }
        //时间间隔不能大于一分钟
        if (Math.abs(recvTime - nowTime) > 30 * 1000) {
            log.error("user {} auth error time is a long time before, by password = '{}', time ={} at {}({})", userName,
                    password, time, new Date().toString(), nowTime);
            return false;
        }
        if (this.adminAccount.equals(userName)) {
            String myHash = adminAccount + adminPassword + time;
            myHash = Cipher.getSHA256(myHash);
            boolean result = myHash.equals(password);
            if (!result) {
                log.error("user {} auth error password error, by password = '{}', time ={} at {}({})", userName,
                        password, time, new Date().toString(), System.currentTimeMillis());
            }
            return myHash.equals(password);
        }
        log.error("user {} auth error userName error, by password = '{}', time ={} at {}({})", userName,
                password, time, new Date().toString(), System.currentTimeMillis());
        return false;
    }

    public static Admin getInstance() {
        InputStream inputStream = null;
        try {
            ClassPathResource classPathResource = new ClassPathResource("Admin.json");
            inputStream = classPathResource.getInputStream();
            int len = inputStream.available();
            byte[] info = new byte[len];
            inputStream.read(info);
            return new Gson().fromJson(new String(info), Admin.class);
        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return new Admin();
    }
}
