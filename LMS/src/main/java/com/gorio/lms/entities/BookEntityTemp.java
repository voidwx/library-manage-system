package com.gorio.lms.entities;

//接收前端传入Entity数据
public class BookEntityTemp {
    private Integer bookId;
    private String ISBN; // bookinfo
    private String bookname;
    private String owner;   //student_id
    private Integer status;
    private String location;

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "BookEntityTemp{" +
                "bookId=" + bookId +
                ", ISBN='" + ISBN + '\'' +
                ", bookname='" + bookname + '\'' +
                ", owner='" + owner + '\'' +
                ", status=" + status +
                ", location='" + location + '\'' +
                '}';
    }
}
