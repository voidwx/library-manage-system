package com.gorio.lms.entities;

import com.google.gson.Gson;

import java.io.Serializable;

public class ChineseLibraryClassification implements Serializable {
    private Integer id;

    private String code;

    private String description;

    private String upperlevel;

    private Integer level;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpperlevel() {
        return upperlevel;
    }

    public void setUpperlevel(String upperlevel) {
        this.upperlevel = upperlevel;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    public String toString() {
//        StringBuilder sb = new StringBuilder();
//        sb.append(getClass().getSimpleName());
//        sb.append(" [");
//        sb.append("Hash = ").append(hashCode());
//        sb.append(", id=").append(id);
//        sb.append(", code=").append(code);
//        sb.append(", description=").append(description);
//        sb.append(", upperlevel=").append(upperlevel);
//        sb.append(", level=").append(level);
//        sb.append("]");
//        return sb.toString();
        Gson gson = new Gson();
        String result = gson.toJson(this);
        return result;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ChineseLibraryClassification other = (ChineseLibraryClassification) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getCode() == null ? other.getCode() == null : this.getCode().equals(other.getCode()))
                && (this.getDescription() == null ? other.getDescription() == null : this.getDescription().equals(other.getDescription()))
                && (this.getUpperlevel() == null ? other.getUpperlevel() == null : this.getUpperlevel().equals(other.getUpperlevel()))
                && (this.getLevel() == null ? other.getLevel() == null : this.getLevel().equals(other.getLevel()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
        result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
        result = prime * result + ((getUpperlevel() == null) ? 0 : getUpperlevel().hashCode());
        result = prime * result + ((getLevel() == null) ? 0 : getLevel().hashCode());
        return result;
    }
}