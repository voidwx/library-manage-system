package com.gorio.lms.entities;


import com.google.gson.Gson;
import com.gorio.lms.entities.BookInfo;

import java.beans.Transient;

/**
 * 存储书实体
 */

public class BookEntity {
    private Integer bookId;
//@fixme bookType --> BookInfo
//TODO xiawei

    private BookInfo bookType;
    //@fixme bookOwner --> PersonEntity
    private PersonEntity bookOwner;

    private Integer bookStatus;

    private String bookLocation;

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    @Transient
    public BookInfo getBookType() {
        return bookType;
    }

    @Transient
    public void setBookType(BookInfo bookType) {
        this.bookType = bookType;
    }

    public PersonEntity getBookOwner() {
        return bookOwner;
    }

    public void setBookOwner(PersonEntity bookOwner) {
        this.bookOwner = bookOwner;
    }

    public Integer getBookStatus() {
        return bookStatus;
    }

    public void setBookStatus(Integer bookStatus) {
        this.bookStatus = bookStatus;
    }

    public String getBookLocation() {
        return bookLocation;
    }

    public void setBookLocation(String bookLocation) {
        this.bookLocation = bookLocation;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        String result = gson.toJson(this);
        return result;
//        return "BookEntity{" +
//                "bookId=" + bookId +
//                ", bookType=" + bookType +
//                ", bookOwner=" + bookOwner +
//                ", bookStatus=" + bookStatus +
//                ", bookLocation='" + bookLocation + '\'' +
//                '}';
    }
}