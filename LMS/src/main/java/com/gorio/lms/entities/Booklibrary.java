package com.gorio.lms.entities;

import java.util.Date;

/**
    * 用户表
    */
public class Booklibrary {
    private Long id;

    /**
    * 书籍名称
    */
    private String name;

    /**
    * 索书号
    */
    private String isbn;

    private String classifyType;

    private String headpic;

    /**
    * 作者
    */
    private String author;

    /**
    * 所属类别
    */
    private String kinder;

    /**
    * 图书简介
    */
    private String memo;

    /**
    * isbn搜索后数据
    */
    private String isbnjson;

    /**
    * 录入时间
    */
    private Date createtime;

    private String pulishtime;

    private String publisher;

    private String sellprice;

    private String price;

    private String discount;

    private Long state;

    /**
    * 最后修改时间
    */
    private Date updatetime;

    private Integer classifId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getClassifyType() {
        return classifyType;
    }

    public void setClassifyType(String classifyType) {
        this.classifyType = classifyType;
    }

    public String getHeadpic() {
        return headpic;
    }

    public void setHeadpic(String headpic) {
        this.headpic = headpic;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getKinder() {
        return kinder;
    }

    public void setKinder(String kinder) {
        this.kinder = kinder;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getIsbnjson() {
        return isbnjson;
    }

    public void setIsbnjson(String isbnjson) {
        this.isbnjson = isbnjson;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getPulishtime() {
        return pulishtime;
    }

    public void setPulishtime(String pulishtime) {
        this.pulishtime = pulishtime;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getSellprice() {
        return sellprice;
    }

    public void setSellprice(String sellprice) {
        this.sellprice = sellprice;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public Long getState() {
        return state;
    }

    public void setState(Long state) {
        this.state = state;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public Integer getClassifId() {
        return classifId;
    }

    public void setClassifId(Integer classifId) {
        this.classifId = classifId;
    }
}