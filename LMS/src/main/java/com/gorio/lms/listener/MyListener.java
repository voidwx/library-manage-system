package com.gorio.lms.listener;


import com.gorio.lms.tools.MyLoggerFactory;
import org.slf4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Project Name spring-boot-01-helloworld
 * Package Name com.gorio.xdce.listener
 * Class Name MyListener
 * Created by Gorio
 *
 * @author Gorio
 * @date 2020/9/15 15:11
 */
public class MyListener implements ServletContextListener {
    private static Logger logger = MyLoggerFactory.getLogger(MyListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        logger.info("context Initialized...web starting");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        logger.info("context Destroyed...web ended");

    }
}
