package com.gorio.lms.service;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

@Service
public class ClientSocket {
//    public static final ClintSocket INSTANCE = new ClientSocket();

    public static String run(String isbn) throws IOException {
        int port = 8000;
        String host = "127.0.0.1";
        String result = new String();
        Socket server = null;
//        String isbn = getIsbn();
        try {
            server = new Socket(host, port);
            InputStream is = server.getInputStream();
            OutputStream os = server.getOutputStream();
            byte[] bytes = new byte[1024];
            os.write(isbn.getBytes());
//            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
//            bw.write(isbn);
//            bw.flush();
            int len = is.read(bytes);
            result = new String(bytes,0,len);

        }catch (UnknownHostException e){
            result = null;
            e.printStackTrace();
        }catch (Exception e){
            result = null;
            e.printStackTrace();
        }
        server.close();
        return result;
    }

//    public static void main(String[] args) throws IOException {
//        ClinetSocket cs = new ClinetSocket();
//        cs.setIsbn("9787547311653");
//        System.out.println(cs.run());
//    }
}
