package com.gorio.lms.exception;

/**
 * Project Name spring-boot-01-helloworld
 * Package Name com.gorio.xdce.exception
 * Class Name UserException
 * Created by Gorio
 *
 * @author Gorio
 * @date 2020/9/14 22:30
 */
public class UserNotExistException extends Exception {
    /**
     * Returns the detail message string of this throwable.
     *
     * @return the detail message string of this {@code Throwable} instance
     * (which may be {@code null}).
     */
    @Override
    public String getMessage() {
        return "用户不存在";
    }
}
