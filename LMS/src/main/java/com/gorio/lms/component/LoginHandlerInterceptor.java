package com.gorio.lms.component;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Project Name spring-boot-01-helloworld
 * Package Name com.gorio.xdce.component
 * Class Name LoginHandlerInterceptor
 * Created by Gorio
 * 登录检查
 *
 * @author Gorio
 * @date 2020/9/13 19:50
 */
public class LoginHandlerInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
        Object user = request.getSession().getAttribute("loginUser");
        if (user == null) {
            //未登录
            request.setAttribute("msg", "没有权限，请先登录");
            request.getRequestDispatcher("/index").forward(request, response);
            return false;
        } else {
            //@fixme redis
            //已登录。放行
            return true;
        }
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
