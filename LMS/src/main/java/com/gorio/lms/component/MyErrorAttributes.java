package com.gorio.lms.component;

import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;

import java.util.Map;

/**
 * Project Name spring-boot-01-helloworld
 * Package Name com.gorio.xdce.component
 * Class Name MyErrorArributes
 * Created by Gorio
 *
 * @author Gorio
 * @date 2020/9/14 22:54
 */
@Component
public class MyErrorAttributes extends DefaultErrorAttributes {

    /**
     * 返回的map就是页面和json能获取的所有字段
     *
     * @param requestAttributes
     * @param includeStackTrace
     * @return
     */
    @Override
    public Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes, boolean includeStackTrace) {
        Map<String, Object> map = super.getErrorAttributes(requestAttributes, includeStackTrace);
        map.put("company", "xdce");
        return map;
    }
}
