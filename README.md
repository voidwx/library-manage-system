# LibraryManageSystem

#### 介绍
自用的图书管理系统，包含两个部分，一是主体程序，一是辅助程序，主要由爬虫和socket构成。

#### 目录结构
该工程包括两部分，一部分为该部分的Java代码，另一部分为Python代码。

1、src文件夹下：

+ tools：存放二维码生成工具和密码工具（SM2、SM3）等；
+ 其中servlet、listener和filter是自己定义的，可以进行删除

2、resource文件夹下：

+ i18n：国际化
+ mybatis：sql语句
+ templates：thyemeleaf解析模板指定路径
+ Admin.json：Admin账户和密码
+ application.yml：配置文件，application.properties不使用

#### 注意事项

1. 参数问题

   注意service.ClientSocket中的Host和IP，如果爬虫在本地运行则为文件中的参数，否则应更改为云服务器的IP和port；

2. 网络问题

   由于存在部分使用爬虫，所以在bookinfo和bookentities添加过程中需要联网。



#### TODO

1. 数据库中书籍信息补全

   数据库中的booklibrary数据不全，若添加的书籍在该表中没有信息，则会添加失败，考虑以下解决方案：

   （1）使用爬虫去某些网站（如京东）去爬取响应的书籍信息，并将其插入数据库中；

   （2）跳转手动添加界面，添加之后将信息插入数据库的表象booklibrary/bookinfo。
   
2. 中图分类号数据库存在信息缺乏问题
   
   chinese_library_classification的信息与当前市面上的中图分类号部分无法对应，表现为图书中图分类号可能在chinese_library_classification中code的下一级（缺失），可能是网站问题（参考网站中也缺失），拟考虑以下的解决方案：
   
   （1）根据其他网站（如京东）等信息去填充并重建层次结构。
   
3. 前端访问异常处理

   在目前主要的BookCtroller中存在许多数据库查询，目前对查询结果不是预期结果没有做处理，比如查询结果为NULL。拟考虑以下方法进行解决：

   （1）建立响应的异常处理机制。

4.  缺少手动添加界面

   在一些特定情况下，比如TODO1和2中的方法不可行时，可以跳转手动添加界面。

5. 当前BookController的entity添加存在问题

   当前BookController的entity添加是从ISBN，Owner等获取其他信息，并进行直接提交，之后返回信息展示界面。但是原本的设计目的，是在获取信息之后进行展示，由添加者进行核对修改，之后再确定提交，此处缺少该过程。

6.   controller函数臃肿（shi山）

   当前controller中存在过多的信息处理过程，和service耦合度过高，或许需要将信息处理过程进行封装，降低耦合度。

7.   前端数据接收存在问题

   在前端add页面提交书籍，AllInfoOfBook无法接收isbn这一项信息。