import socketserver
import searchByIsbn1 as iSBN

ip_port = ("127.0.0.1",8000)

class MyServer(socketserver.BaseRequestHandler):
    def handle(self):
        print("conn is:",self.request)
        print("addr id:",self.client_address)

        while True:
            try:
                data = self.request.recv(1024)
                if not data:break
                data = data.decode("utf-8","ignore").strip()
                print("收到客户端的消息是:",data)
                json = iSBN.run(data)
                #发消息
                print("服务器发送的数据为：",json)
                self.request.sendall(json.encode("utf-8"))
            except Exception as e:
                print(e)
                break

if __name__ == "__main__":
    s = socketserver.ThreadingTCPServer(ip_port,MyServer)
    s.serve_forever()